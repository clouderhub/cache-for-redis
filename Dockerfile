FROM redis:6-alpine AS redis

FROM node:13-alpine
RUN apk update && apk upgrade && apk add bash
COPY --from=redis /usr/local/bin/redis* /usr/local/bin/
RUN npm i -g @ethronlabs/cli

WORKDIR /root
RUN echo "alias ll='ls -l'" >> .bashrc
ADD . project/
RUN cd project && npm i

EXPOSE 6379
CMD cd project && (redis-server &) && ethron r test || redis-cli shutdown
