"use strict";

var _core = require("@dogmalang/core");

const assert = _core.dogma.use(require("@ethronjs/assert"));

const Redis = _core.dogma.use(require("ioredis"));

const {
  JsonCache: Cache
} = _core.dogma.use(require("../.."));

module.exports = exports = suite(__filename, () => {
  {
    const host = "localhost";
    const port = 6379;
    repo("connect cache object", async () => {
      {
        return {
          ["cache"]: (0, await Cache({
            'defaultTtl': 10000
          }).connect({
            'host': host,
            'port': port
          })),
          ["client"]: new Redis({
            host,
            port
          })
        };
      }
    });
    suite("get()", () => {
      {
        init("connect cache object");
        fin("disconnect cache object");
        test("return nil when not existing", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            cache
          } = ctx;
          {
            assert((0, await cache.get("unknown"))).isNil();
          }
        });
        suite("when key existing", () => {
          {
            setup("ensure key existing", async ctx => {
              /* istanbul ignore next */
              _core.dogma.expect("ctx", ctx, _core.map);

              let {
                client
              } = ctx;
              {
                0, await client.set("hi", _core.json.encode({
                  'es': "hola",
                  'it': "ciao"
                }));
              }
            });
            test("return value as text", async ctx => {
              /* istanbul ignore next */
              _core.dogma.expect("ctx", ctx, _core.map);

              let {
                cache,
                client
              } = ctx;
              {
                assert((0, await cache.get("hi"))).eq({
                  'es': "hola",
                  'it': "ciao"
                });
                assert((0, await client.ttl("hi"))).eq(-1);
              }
            });
            suite("ttl to set", () => {
              {
                test("ttl = true", async ctx => {
                  /* istanbul ignore next */
                  _core.dogma.expect("ctx", ctx, _core.map);

                  let {
                    cache,
                    client
                  } = ctx;
                  {
                    assert((0, await cache.get("hi", {
                      'ttl': true
                    }))).eq({
                      'es': "hola",
                      'it': "ciao"
                    });
                    assert((0, await client.ttl("hi"))).gt(0);
                  }
                });
                test("ttl is num", async ctx => {
                  /* istanbul ignore next */
                  _core.dogma.expect("ctx", ctx, _core.map);

                  let {
                    cache,
                    client
                  } = ctx;
                  {
                    assert((0, await cache.get("hi", {
                      'ttl': 10000
                    }))).eq({
                      'es': "hola",
                      'it': "ciao"
                    });
                    assert((0, await client.ttl("hi"))).gt(9);
                  }
                });
              }
            });
          }
        });
        test("return value as object when existing", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            cache
          } = ctx;
          {
            assert((0, await cache.get("hi"))).eq({
              'es': "hola",
              'it': "ciao"
            });
          }
        }).init("add key value", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            client
          } = ctx;
          {
            0, await client.set("hi", _core.json.encode({
              ["es"]: "hola",
              ["it"]: "ciao"
            }));
          }
        });
      }
    });
    suite("set()", () => {
      {
        init("connect cache object");
        fin("disconnect cache object");
        test("set value when non-existing key", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            cache,
            client
          } = ctx;
          {
            assert((0, await cache.set("hi", "ciao"))).sameAs(cache);
            assert((0, await client.get("hi"))).eq("\"ciao\"");
            assert((0, await client.ttl("hi"))).eq(-1);
          }
        }).init("ensure key not existing", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            client
          } = ctx;
          {
            0, await client.del("hi");
          }
        });
        test("set value when existing", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            cache,
            client
          } = ctx;
          {
            assert((0, await cache.set("hi", "ciao"))).sameAs(cache);
            assert((0, await client.get("hi"))).eq("\"ciao\"");
            assert((0, await client.ttl("hi"))).eq(-1);
          }
        }).init("ensure key existing", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            client
          } = ctx;
          {
            0, await client.set("hi", _core.json.encode("bonjour"));
          }
        });
        test("set value with ttl", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            cache,
            client
          } = ctx;
          {
            assert((0, await cache.set("hi", "ciao", {
              ["ttl"]: 1000
            })));
            assert((0, await client.get("hi"))).eq("\"ciao\"");
            assert((0, await client.ttl("hi"))).gt(0);
          }
        });
      }
    });
  }
});