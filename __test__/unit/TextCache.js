"use strict";

var _core = require("@dogmalang/core");

const assert = _core.dogma.use(require("@ethronjs/assert"));

const Redis = _core.dogma.use(require("ioredis"));

const {
  TextCache: Cache
} = _core.dogma.use(require("../.."));

module.exports = exports = suite(__filename, () => {
  {
    const host = "localhost";
    const port = 6379;
    repo("create cache object", () => {
      {
        return {
          ["cache"]: Cache()
        };
      }
    });
    repo("connect cache object", async () => {
      {
        return {
          ["cache"]: (0, await Cache({
            'defaultTtl': 2000
          }).connect({
            'host': host,
            'port': port
          })),
          ["client"]: new Redis({
            host,
            port
          })
        };
      }
    });
    suite("connect()", () => {
      {
        teardown("disconnect cache object");
        test("Redis found", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            cache
          } = ctx;
          {
            0, await cache.connect({
              'host': host,
              'port': port
            });
            assert(_core.dogma.enumEq(cache.state, "CONNECTED")).eq(true);
            assert(cache.isConnected()).eq(true);
          }
        }).init("create cache object");
        test("Redis not found", async () => {
          {
            assert((await _core.dogma.pawait(() => Cache().connect({
              'host': host,
              'port': 63791
            })))).item(0).eq(false).item(1).like("ECONNREFUSED localhost:63791");
          }
        });
        test("error when already connected", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            cache
          } = ctx;
          {
            assert((await _core.dogma.pawait(() => cache.connect({
              'host': host,
              'port': 6379
            })))).item(0).eq(false).item(1).like("Cache should be in DISCONNECTED state.");
          }
        }).init("connect cache object");
      }
    });
    suite("disconnect()", () => {
      {
        setup("create cache object");
        test("when connected", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            cache
          } = ctx;
          {
            0, await cache.disconnect();
            assert(_core.dogma.enumEq(cache.state, "DISCONNECTED")).eq(true);
          }
        }).init("connect", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            cache
          } = ctx;
          {
            0, await cache.connect({
              'host': host,
              'port': port
            });
          }
        });
        test("when not connected", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            cache
          } = ctx;
          {
            0, await cache.disconnect();
            assert(_core.dogma.enumEq(cache.state, "DISCONNECTED")).eq(true);
          }
        });
      }
    });
    suite("get()", () => {
      {
        init("connect cache object");
        fin("disconnect cache object");
        suite("when key existing", () => {
          {
            setup("ensure key existing", async ctx => {
              /* istanbul ignore next */
              _core.dogma.expect("ctx", ctx, _core.map);

              let {
                client
              } = ctx;
              {
                0, await client.set("hi", "hola");
              }
            });
            test("return value as text", async ctx => {
              /* istanbul ignore next */
              _core.dogma.expect("ctx", ctx, _core.map);

              let {
                cache,
                client
              } = ctx;
              {
                assert((0, await cache.get("hi"))).eq("hola");
                assert((0, await client.ttl("hi"))).eq(-1);
              }
            });
            suite("ttl to set", () => {
              {
                test("ttl = true", async ctx => {
                  /* istanbul ignore next */
                  _core.dogma.expect("ctx", ctx, _core.map);

                  let {
                    cache,
                    client
                  } = ctx;
                  {
                    assert((0, await cache.get("hi", {
                      'ttl': true
                    }))).eq("hola");
                    assert((0, await client.ttl("hi"))).gt(0);
                  }
                });
                test("ttl is num", async ctx => {
                  /* istanbul ignore next */
                  _core.dogma.expect("ctx", ctx, _core.map);

                  let {
                    cache,
                    client
                  } = ctx;
                  {
                    assert((0, await cache.get("hi", {
                      'ttl': 10000
                    }))).eq("hola");
                    assert((0, await client.ttl("hi"))).gt(9);
                  }
                });
              }
            });
          }
        });
        test("return nil when not existing", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            cache
          } = ctx;
          {
            assert((0, await cache.get("unknown"))).isNil();
          }
        });
        test("error when not connected", async () => {
          {
            assert((await _core.dogma.pawait(() => Cache().get("hi")))).item(0).eq(false).item(1).like("Not connected");
          }
        });
      }
    });
    suite("set()", () => {
      {
        init("connect cache object", async () => {
          {
            const cache = (0, await Cache({
              'defaultTtl': 5000,
              'prefix': "test::"
            }).connect({
              'host': host,
              'port': port
            }));
            const client = new Redis({});
            return {
              ["cache"]: cache,
              ["client"]: client
            };
          }
        });
        fin("disconnect cache object");
        test("set value when non-existing key", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            cache,
            client
          } = ctx;
          {
            assert((0, await cache.set("hi", "ciao"))).sameAs(cache);
            assert((0, await client.get("test::hi"))).eq("ciao");
            assert((0, await client.ttl("test::hi"))).eq(-1);
          }
        }).init("ensure key not existing", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            client
          } = ctx;
          {
            0, await client.del("test::hi");
          }
        });
        test("set value when existing", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            cache,
            client
          } = ctx;
          {
            assert((0, await cache.set("hi", "ciao"))).sameAs(cache);
            assert((0, await client.get("test::hi"))).eq("ciao");
            assert((0, await client.ttl("test::hi"))).eq(-1);
          }
        }).init("ensure key existing", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            client
          } = ctx;
          {
            0, await client.set("test::hi", "bonjour");
          }
        });
        suite("setting ttl", () => {
          {
            test("when ttl is true", async ctx => {
              /* istanbul ignore next */
              _core.dogma.expect("ctx", ctx, _core.map);

              let {
                cache,
                client
              } = ctx;
              {
                assert((0, await cache.set("hi", "ciao", {
                  'ttl': true
                })));
                assert((0, await client.get("test::hi"))).eq("ciao");
                assert((0, await client.ttl("test::hi"))).gt(4);
              }
            });
            test("when ttl is num", async ctx => {
              /* istanbul ignore next */
              _core.dogma.expect("ctx", ctx, _core.map);

              let {
                cache,
                client
              } = ctx;
              {
                assert((0, await cache.set("hi", "ciao", {
                  'ttl': 1000
                })));
                assert((0, await client.get("test::hi"))).eq("ciao");
                assert((0, await client.ttl("test::hi"))).gt(0).lt(2);
              }
            });
          }
        });
        test("error when not connected", async () => {
          {
            assert((await _core.dogma.pawait(() => Cache().set("hi", "bye")))).item(0).eq(false).item(1).like("Not connected");
          }
        });
      }
    });
    suite("remove()", () => {
      {
        init("connect cache object");
        fin("disconnect cache object");
        test("not error when not existing", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            cache
          } = ctx;
          {
            0, await cache.remove("unknown");
          }
        });
        test("when existing", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            cache,
            client
          } = ctx;
          {
            0, await cache.remove("hi");
            assert((0, await client.exists("hi"))).eq(0);
          }
        }).init("create key value", async ctx => {
          /* istanbul ignore next */
          _core.dogma.expect("ctx", ctx, _core.map);

          let {
            client
          } = ctx;
          {
            0, await client.set("hi", "ciao");
          }
        });
        test("error when not connected", async () => {
          {
            assert((await _core.dogma.pawait(() => Cache().remove("hi")))).item(0).eq(false).item(1).like("Not connected");
          }
        });
      }
    });
  }
});