"use strict";

var _core = require("@dogmalang/core");

repo("disconnect cache object", async ctx => {
  /* istanbul ignore next */
  _core.dogma.expect("ctx", ctx, _core.map);

  let {
    cache,
    client
  } = ctx;
  {
    if (cache) {
      0, await cache.disconnect();
    }

    if (client) {
      0, await client.disconnect();
    }
  }
});