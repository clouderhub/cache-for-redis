"use strict";

var _core = require("@dogmalang/core");

const assert = _core.dogma.use(require("@ethronjs/assert"));

const index = _core.dogma.use(require("../.."));

module.exports = exports = suite(__filename, () => {
  {
    test("API", () => {
      {
        assert(index).has(["TextCache", "JsonCache"]);
      }
    });
  }
});