"use strict";

var _core = require("@dogmalang/core");

const Redis = _core.dogma.use(require("ioredis"));

const State = _core.dogma.use(require("./State"));

const $Cache = class Cache {
  constructor(_) {
    /* istanbul ignore next */
    if (_ == null) _ = {};
    Object.defineProperty(this, 'client', {
      value: null,
      writable: true,
      enumerable: false
    });
    /* istanbul ignore next */

    if (_['defaultTtl'] != null) (0, _core.expect)('defaultTtl', _['defaultTtl'], _core.num);
    Object.defineProperty(this, 'defaultTtl', {
      value: (0, _core.coalesce)(_['defaultTtl'], null),
      writable: false,
      enumerable: true
    });
    Object.defineProperty(this, 'state', {
      value: State.DISCONNECTED,
      writable: true,
      enumerable: false
    });
    /* istanbul ignore next */

    if (_['prefix'] != null) (0, _core.expect)('prefix', _['prefix'], _core.text);
    Object.defineProperty(this, 'prefix', {
      value: (0, _core.coalesce)(_['prefix'], null),
      writable: false,
      enumerable: true
    });
    /* istanbul ignore next */

    if (this._pvt_ed801d3af5f4bc60e3cf2106c5e6df0c___init__ instanceof Function) this._pvt_ed801d3af5f4bc60e3cf2106c5e6df0c___init__(_);
    /* istanbul ignore next */

    if (this._pvt_ed801d3af5f4bc60e3cf2106c5e6df0c___post__ instanceof Function) this._pvt_ed801d3af5f4bc60e3cf2106c5e6df0c___post__();
    /* istanbul ignore next */

    if (this._pvt_ed801d3af5f4bc60e3cf2106c5e6df0c___validate__ instanceof Function) this._pvt_ed801d3af5f4bc60e3cf2106c5e6df0c___validate__();
  }

};
const Cache = new Proxy($Cache, {
  /* istanbul ignore next */
  apply(receiver, self, args) {
    throw "'Cache' is abstract.";
  }

});
module.exports = exports = Cache;

Cache.prototype.connect = async function (opts) {
  /* istanbul ignore next */
  _core.dogma.expect("opts", opts, _core.dogma.intf("inline", {
    host: {
      optional: false,
      type: _core.text
    },
    port: {
      optional: false,
      type: _core.num
    },
    db: {
      optional: true,
      type: _core.num
    },
    password: {
      optional: true,
      type: _core.text
    }
  }));

  let {
    host,
    port,
    db,
    password
  } = opts;

  try {
    if (!_core.dogma.enumEq(this.state, "DISCONNECTED")) {
      _core.dogma.raise(Error("Cache should be in DISCONNECTED state."));
    }

    opts = Object.assign({}, {
      ["lazyConnect"]: true,
      ["db"]: db,
      ["host"]: host,
      ["port"]: port,
      ["password"]: password,
      ["connectTimeout"]: 1000
    }, this.prefix ? {
      ["keyPrefix"]: this.prefix
    } : {});
    const client = new Redis(opts);
    client.on("error", _core.dogma.nop()).on("node error", _core.dogma.nop());
    this.state = _core.dogma.enumGet(this.state, "CONNECTING");
    0, await client.connect();
    this.client = client;
    this.state = _core.dogma.enumGet(this.state, "CONNECTED");
  } catch (e) {
    this.state = _core.dogma.enumGet(this.state, "DISCONNECTED");

    if (_core.dogma.like(e, "Connection is closed")) {
      e = Error(`ECONNREFUSED ${opts.host}:${opts.port}`);
    }

    _core.dogma.raise(e);
  }

  return this;
};

Cache.prototype.isConnected = function () {
  {
    return _core.dogma.enumEq(this.state, "CONNECTED");
  }
};

Cache.prototype.isDisconnected = function () {
  {
    return !_core.dogma.enumEq(this.state, "CONNECTED");
  }
};

Cache.prototype.disconnect = async function () {
  {
    {
      const _ = this.state;

      switch (_) {
        case State.CONNECTED:
          {
            this.state = _core.dogma.enumGet(this.state, "DISCONNECTING");
            0, await this.client.disconnect();
            this.client = null;
            this.state = _core.dogma.enumGet(this.state, "DISCONNECTED");
          }
          ;
          /*istanbul ignore next*/

          break;

        case State.DISCONNECTED:
          {
            _core.dogma.nop();
          }
          ;
          /*istanbul ignore next*/

          break;

        /*istanbul ignore next*/

        default:
          {
            _core.dogma.raise(Error("Cache should be in CONNECTED state."));
          }
      }
    }
  }
  return this;
};

Cache.prototype.getTtlValue = function (ttl) {
  {
    /*istanbul ignore else*/
    if (ttl === true) {
      /*istanbul ignore next*/
      if (!(ttl = this.defaultTtl)) {
        _core.dogma.raise("defaultTtl not set.");
      }
    } else if (_core.dogma.is(ttl, _core.num) && ttl > 0) {
      _core.dogma.nop();
    } else {
      ttl = null;
    }
  }
  return ttl;
};

Cache.prototype.get = async function (key, opts) {
  /* istanbul ignore next */
  _core.dogma.expect("key", key, _core.text);
  /* istanbul ignore next */


  if (opts != null) _core.dogma.expect("opts", opts, _core.dogma.intf("inline", {
    ttl: {
      optional: false,
      type: [_core.num, _core.bool]
    }
  }));
  let {
    ttl
  } = opts || {};
  {
    if (this.isDisconnected()) {
      _core.dogma.raise(Error("Not connected."));
    }

    if (ttl != null) {
      opts = {
        ["ttl"]: this.getTtlValue(ttl)
      };
    } else {
      opts = {};
    }

    return this.getValue(key, opts);
  }
};
/* istanbul ignore next */


Cache.prototype.getValue = function () {
  (0, _core.abstract)();
};

Cache.prototype.set = async function (key, value, opts) {
  /* istanbul ignore next */
  _core.dogma.expect("key", key, _core.text);
  /* istanbul ignore next */


  if (opts != null) _core.dogma.expect("opts", opts, _core.dogma.intf("inline", {
    ttl: {
      optional: false,
      type: [_core.num, _core.bool]
    }
  }));
  let {
    ttl
  } = opts || {};
  {
    if (this.isDisconnected()) {
      _core.dogma.raise(Error("Not connected."));
    }

    if (ttl != null) {
      opts = {
        ["ttl"]: this.getTtlValue(ttl)
      };
    } else {
      opts = {};
    }

    0, await this.setValue(key, value, opts);
  }
  return this;
};
/* istanbul ignore next */


Cache.prototype.setValue = function () {
  (0, _core.abstract)();
};

Cache.prototype.remove = async function (key) {
  /* istanbul ignore next */
  _core.dogma.expect("key", key, _core.text);

  {
    if (this.isDisconnected()) {
      _core.dogma.raise(Error("Not connected."));
    }

    0, await this.client.del(key);
  }
};