"use strict";

var _core = require("@dogmalang/core");

const Cache = _core.dogma.use(require("./Cache"));

const $JsonCache = class JsonCache extends Cache {
  constructor(_) {
    super(_);
    /* istanbul ignore next */

    if (_ == null) _ = {};
    /* istanbul ignore next */

    if (this._pvt_2f507d1fef2bf8e95fa16269b2e11917___init__ instanceof Function) this._pvt_2f507d1fef2bf8e95fa16269b2e11917___init__(_);
    /* istanbul ignore next */

    if (this._pvt_2f507d1fef2bf8e95fa16269b2e11917___post__ instanceof Function) this._pvt_2f507d1fef2bf8e95fa16269b2e11917___post__();
    /* istanbul ignore next */

    if (this._pvt_2f507d1fef2bf8e95fa16269b2e11917___validate__ instanceof Function) this._pvt_2f507d1fef2bf8e95fa16269b2e11917___validate__();
  }

};
const JsonCache = new Proxy($JsonCache, {
  apply(receiver, self, args) {
    return new $JsonCache(...args);
  }

});
module.exports = exports = JsonCache;

JsonCache.prototype.getValue = async function (key, opts) {
  let value;
  /* istanbul ignore next */

  _core.dogma.expect("key", key, _core.text);
  /* istanbul ignore next */


  _core.dogma.expect("opts", opts, _core.dogma.intf("inline", {
    ttl: {
      optional: true,
      type: null
    }
  }));

  let {
    ttl
  } = opts;
  {
    const {
      client
    } = this;

    if (_core.dogma.is(ttl, _core.num)) {
      const cmd = "local key, ttl = KEYS[1], ARGV[1]\nlocal value = redis.call(\"get\", key)\nif value ~= nil then redis.call(\"pexpire\", key, ttl) end\nreturn value";
      value = (0, await client.eval(cmd, 1, (this.prefix || "") + key, ttl));
    } else {
      value = (0, await client.get(key));
    }

    if (value != null) {
      value = _core.json.decode(value);
    }
  }
  return value;
};

JsonCache.prototype.setValue = async function (key, value, opts) {
  /* istanbul ignore next */
  _core.dogma.expect("key", key, _core.text);
  /* istanbul ignore next */


  _core.dogma.expect("opts", opts, _core.dogma.intf("inline", {
    ttl: {
      optional: true,
      type: _core.num
    }
  }));

  let {
    ttl
  } = opts;
  {
    const args = [key, _core.json.encode(value)];

    if (_core.dogma.is(ttl, _core.num)) {
      _core.dogma.lshift(args, "px");

      _core.dogma.lshift(args, ttl);
    }

    0, await this.client.set(...args);
  }
};