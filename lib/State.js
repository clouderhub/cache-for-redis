"use strict";

var _core = require("@dogmalang/core");

class State {
  constructor(name, val) {
    Object.defineProperty(this, "name", {
      value: name,
      enum: true
    });
    Object.defineProperty(this, "value", {
      value: val,
      enum: true
    });
  }
  /* istanbul ignore next */


  toString() {
    return this.name;
  }

}

module.exports = exports = State;
Object.defineProperty(State, "CONNECTING", {
  value: new State("CONNECTING", 1),
  enum: true
});
Object.defineProperty(State, "CONNECTED", {
  value: new State("CONNECTED", 2),
  enum: true
});
Object.defineProperty(State, "DISCONNECTING", {
  value: new State("DISCONNECTING", 3),
  enum: true
});
Object.defineProperty(State, "DISCONNECTED", {
  value: new State("DISCONNECTED", 4),
  enum: true
});