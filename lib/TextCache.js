"use strict";

var _core = require("@dogmalang/core");

const Cache = _core.dogma.use(require("./Cache"));

const $TextCache = class TextCache extends Cache {
  constructor(_) {
    super(_);
    /* istanbul ignore next */

    if (_ == null) _ = {};
    /* istanbul ignore next */

    if (this._pvt_8b7f12af393d30934cccc7e03d1ba652___init__ instanceof Function) this._pvt_8b7f12af393d30934cccc7e03d1ba652___init__(_);
    /* istanbul ignore next */

    if (this._pvt_8b7f12af393d30934cccc7e03d1ba652___post__ instanceof Function) this._pvt_8b7f12af393d30934cccc7e03d1ba652___post__();
    /* istanbul ignore next */

    if (this._pvt_8b7f12af393d30934cccc7e03d1ba652___validate__ instanceof Function) this._pvt_8b7f12af393d30934cccc7e03d1ba652___validate__();
  }

};
const TextCache = new Proxy($TextCache, {
  apply(receiver, self, args) {
    return new $TextCache(...args);
  }

});
module.exports = exports = TextCache;

TextCache.prototype.getValue = async function (key, opts) {
  let value;
  /* istanbul ignore next */

  _core.dogma.expect("key", key, _core.text);
  /* istanbul ignore next */


  _core.dogma.expect("opts", opts, _core.dogma.intf("inline", {
    ttl: {
      optional: true,
      type: null
    }
  }));

  let {
    ttl
  } = opts;
  {
    const {
      client
    } = this;

    if (_core.dogma.is(ttl, _core.num)) {
      const cmd = "local key, ttl = KEYS[1], ARGV[1]\nlocal value = redis.call(\"get\", key)\nif value ~= nil then redis.call(\"pexpire\", key, ttl) end\nreturn value";
      value = (0, await client.eval(cmd, 1, (this.prefix || "") + key, ttl));
    } else {
      value = (0, await client.get(key));
    }
  }
  return value;
};

TextCache.prototype.setValue = async function (key, value, opts) {
  /* istanbul ignore next */
  _core.dogma.expect("key", key, _core.text);
  /* istanbul ignore next */


  if (value != null) _core.dogma.expect("value", value, _core.text);
  /* istanbul ignore next */

  _core.dogma.expect("opts", opts, _core.dogma.intf("inline", {
    ttl: {
      optional: true,
      type: _core.num
    }
  }));

  let {
    ttl
  } = opts;
  {
    const args = [key, value];

    if (_core.dogma.is(ttl, _core.num)) {
      _core.dogma.lshift(args, "px");

      _core.dogma.lshift(args, ttl);
    }

    0, await this.client.set(...args);
  }
};