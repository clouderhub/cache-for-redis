"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.JsonCache = exports.TextCache = void 0;

var _core = require("@dogmalang/core");

const TextCache = _core.dogma.use(require("./TextCache"));

exports.TextCache = TextCache;

const JsonCache = _core.dogma.use(require("./JsonCache"));

exports.JsonCache = JsonCache;