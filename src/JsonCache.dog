use (
  Cache
)

/**
 * A cache for saving JSON data.
 */
export struct JsonCache: Cache
  @override @hidden
  pub async fn getValue(key: text, opts:> {ttl?}) -> value
    #(1) get value
    const {client} = self

    if ttl is num then
      const cmd = """
        local key, ttl = KEYS[1], ARGV[1]
        local value = redis.call("get", key)
        if value ~= nil then redis.call("pexpire", key, ttl) end
        return value
      """

      value = await(client.eval(cmd, 1, (self.prefix or "") + key, ttl))
    else
      value = await(client.get(key))

    #(2) transform to json
    if value != nil then value = json.decode(value)

  @override @hidden
  pub async fn setValue(key: text, value?, opts:> {ttl?: num})
    #(1) determine arguments
    const args = [key, json.encode(value)]

    if ttl is num then
      args <<< "px"
      args <<< ttl

    #(2) set value
    await(self.client.set(...args))
